#!/usr/bin/env python

import getopt
import shutil
import time
import sys
import os

from ltapublish.utils import Config, write_mapfile
from ext_data import get_ext_data
from subprocess32 import PIPE, Popen, TimeoutExpired

usage = """
Usage:
    ltamapfile [options] cera_acronym_list
        or
    ltamapfile [options] cera_acronym

    Creates ESGF mapfiles for CERA datasetgroups. Output one mapfile per ESGF dataset

Options:
    -h, --help
        Print this help message

    --cman
        Use connection manager for database connect.

    -i init_dir
        Directory containing all initialization files.
        Recommended: one initialization file for the default sections (esg.ini) and one per project, must match the name format esg.<project>.ini
        If not specified, the default installed init files are read.

    -o, --out out_path
        Create one mapfile per ESGF dataset to out_path.
        If not specified generate directory mapfile_YYYYMMDD to working directory.

    --project project
        Project identifier. This option is mandatory.

    --replica
        Add flag "replica=True" to dataset.

"""


def main(argv):

    try:
        args, lastargs = getopt.getopt(argv, "ho:i:", ['help', 'cman', 'out=', 'project=', 'replica'])
    except getopt.error:
        print sys.exc_value
        print usage
        sys.exit(0)

    acronym_list = []
    if len(lastargs) != 0 and os.path.isfile(lastargs[0]):
        acronym_file = open(lastargs[0], 'r')
        for acronym in acronym_file:
            acronym_list.append(acronym.split('\n')[0])
    elif len(lastargs) != 0:
        acronym_list = lastargs
    else:
        print usage
        sys.exit(0)

    timestamp = time.time()
    cman = False
    init_dir = '/esg/config/esgcet/'
    out_dir = None
    project = None
    is_replica = False

    for flag, arg in args:
        if flag in ['-h', '--help']:
            print usage
            sys.exit(0)
        elif flag == '--cman':
            cman = True
        elif flag == '-i':
            init_dir = arg
        elif flag in ['-o', '--out']:
            out_dir = arg
        elif flag == '--project':
            project = arg.lower()
        elif flag == '--replica':
            is_replica = True

    # The project must be specified
    if project is None:
        raise 'Must specify project with --project'

    if len(acronym_list) > 0:
        tmp_dir_root = '/tmp/ltamapfile_tmp_%s' % timestamp
        os.mkdir(tmp_dir_root)
        status_num = 0
        for acronym in acronym_list:
            status_num += 1
            print('Processing Acronym %s (%s of %s)' % (acronym, status_num, len(acronym_list)))
            dataset_file_dict = {}
            try:
                out_dir_acronym = os.path.join(out_dir, acronym)
                if not os.path.isdir(out_dir_acronym):
                    os.mkdir(out_dir_acronym, 0775)
                tmp_dir = os.path.join(tmp_dir_root, acronym)
                os.mkdir(tmp_dir)
                meta_export_cmd = [get_ext_data.get_me_location(), '--listfiles', acronym]
                if cman:
                    meta_export_cmd.append('--cman')

                me_files = Popen(meta_export_cmd, stdout=PIPE)
                filelist, err = me_files.communicate(timeout=100)
                for f in filelist.split('\n'):
                    # Case 1: CMIP5
                    # cmip5.output1.BCC.bcc-csm1-1-m.amip.6hr.atmos.6hrPlev.r3i1p1.v20120910.ua.ua_6hrPlev_bcc-csm1-1-m_amip_r3i1p1_197901010000-198012311800.nc
                    if f:
                        if project.upper() == 'CMIP5':
                            dataset_name = '.'.join(f.split('.')[:-4])
                            version = f.split('.')[-4].split('v')[-1]
                            dataset = '%s#%s' % (dataset_name, version)
                            if dataset not in dataset_file_dict:
                                dataset_file_dict[dataset] = set()
                            dataset_file_dict[dataset].add(f)
                            continue
                        else:
                            # TODO! Other projects
                            pass
            except:
                print('Not a valid acronym: %s' % acronym)
                
            for dataset, files in dataset_file_dict.iteritems():
                tmp_indir = os.path.join(tmp_dir, 'input')
                if not os.path.isdir(tmp_indir):
                    os.mkdir(tmp_indir)
                tmp_outdir = os.path.join(tmp_dir, 'output')
                if not os.path.isdir(tmp_outdir):
                    os.mkdir(tmp_outdir)

                tmp_datasetfilename = '%s/%s' % (tmp_indir, dataset)
                tmp_datasetfile = open(tmp_datasetfilename, 'w')
                for f in files:
                    tmp_datasetfile.write('%s\n' % f)
                tmp_datasetfile.close()

                meta_export_cmd = [get_ext_data.get_me_location(), '--batchxml', tmp_datasetfilename, '-d', tmp_outdir]
                if cman:
                    meta_export_cmd.append('--cman')

                batchxml = Popen(meta_export_cmd)

                success = 0
                tries = 0
                while not success:
                    try:
                        batchxml.wait(timeout=600)
                        success = 1
                    except TimeoutExpired:
                        tries += 1
                        print tries

                # load config (default and project config file)
                config = Config(init_dir, project)

                write_mapfile(config, dataset, files, project, is_replica, out_dir_acronym, tmp_outdir)

    # clean up ...
    shutil.rmtree(tmp_dir_root)


if __name__ == '__main__':
    main(sys.argv[1:])
