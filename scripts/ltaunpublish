#!/usr/bin/env python

import getopt
import os
import sys
import socket

from ltapublish.utils import Config, establish_pid_connection, load_mapfile, DBcon
from ltapublish.publish import reinit_thredds, unpublish_thredds, SolrPublicationService
from ltapublish.logon import check_cert, logon, get_myproxy_value_from_config


def unpublish_dataset(dbcon, mapfile, config, project, unpublish_from_thredds, unpublish_from_index, pid_connector, option):
    dataset, pid_wizard = load_mapfile(mapfile, project, config, pid_connector)

    if unpublish_from_index:
        # check cert and generate a new one, if expired
        myproxy_username = get_myproxy_value_from_config(config, 'username')
        myproxy_hostname = get_myproxy_value_from_config(config, 'hostname')
        myproxy_password = get_myproxy_value_from_config(config, 'password')
        if not check_cert(config, myproxy_username):
            print 'Invalid myproxy certificate, renewing...'
            try:
                logon(config, myproxy_username, myproxy_password, myproxy_hostname)
            except:
                print 'Certificate generation failed, please try to run myproxy-logon manually...'
        print 'Unpublish %s from index node.' % dataset.dataset_name
        try:
            solr_pub_service = SolrPublicationService(config)
            dataset_id = '%s.%s|%s' % (dataset.dataset_name, dataset.dataset_version, socket.gethostname())
            solr_pub_service.unpublish(dataset_id, option)
            print 'SUCCESS'
        except:
            print 'FAILED'

    if unpublish_from_thredds:
        dataset, pid_wizard = load_mapfile(mapfile, project, config, pid_connector)
        if pid_connector:
            pid_connector.unpublish_one_version(drs_id=dataset.dataset_name, version_number=dataset.dataset_version)
        location = dbcon.query_postgres(dataset, facet='location')
        print 'Unpublish %s from THREDDS catalog.' % dataset.dataset_name
        if not location:
            print 'Dataset not present in Postgres: %s' % dataset.dataset_name
        else:
            unpublish_thredds(dbcon, config, location, pid_wizard)



usage = """
Usage:
    ltaunpublish [options] dataset_mapfile or mapfile_directory

        Unpublish dataset(s)

Arguments:
    dataset_mapfile: A file containing all files and metadata for a single dataset
    mapfile_directory: A directory containing one or more dataset_mapfiles

Options:
    -h, --help
        Print this help message

    -i init_dir
        Directory containing all initialization files.
        Recommended: one initialization file for the default sections (esg.ini) and one per project, must match the name format esg.<project>.ini
        If not specified, the default installed init files are read.

    --index-only
        Unpublish from the Index node only, keep THREDDS catalog.

    --no-thredds-reinit
        The THREDDS server is not reinitialized, regardless of whether --thredds is used.
        This option is useful to generate one or more new dataset catalogs without the overhead of a TDS reinitialization call,
        followed by a single reinitialization call.
        Use this option with caution, as it will leave the database and THREDDS catalogs in an inconsistent state.

    --project project_id:
        Project identifier. This option is mandatory.

    --test:
        Flag the data as TEST, only for PIDs

    --thredds-only
        Remove only THREDDS catalog, keep Solr record if any.
"""


def main(argv):

    try:
        args, lastargs = getopt.getopt(argv, "hi:", ['delete', 'help', 'index-only', 'no-thredds-reinit', 'project=', 'retract', 'test', 'thredds-only'])
    except getopt.error:
        print sys.exc_value
        print usage
        sys.exit(0)

    init_dir = '/esg/config/esgcet/'
    unpublish_from_index = True
    unpublish_from_thredds = True
    reinitialize_thredds = True
    project = None
    test_publication = False
    option = 'RETRACT'

    for flag, arg in args:
        if flag == '--delete':
            option = 'DELETE'
        elif flag in ['-h', '--help']:
            print usage
            sys.exit(0)
        elif flag == '-i':
            init_dir = arg
        elif flag == '--index-only':
            unpublish_from_thredds = False
        elif flag == '--no-thredds-reinit':
            reinitialize_thredds = False
        elif flag == '--project':
            project = arg
        elif flag == '--retract':
            option = 'RETRACT'
        elif flag == '--test':
            test_publication = True
        elif flag == '--thredds-only':
            unpublish_from_index = False

    # The project must be specified
    if project is None:
        raise 'Must specify project with --project'

    if len(lastargs) == 0:
        print "No mapfile(s) specified."
        print usage
        sys.exit(0)
    else:
        map_arg = str(lastargs[0])
        mapfile = mapfile_dir = None
        if os.path.isfile(map_arg):
            mapfile = map_arg
        elif os.path.isdir(map_arg):
            mapfile_dir = map_arg
        else:
            raise 'Not a valid file or directory: %s' % arg

    if not (unpublish_from_index or unpublish_from_thredds):
        raise 'Can not specify both, --index-only and --thredds-only'

    if not unpublish_from_thredds:
        reinitialize_thredds = False

    # load config (default and project config file)
    config = Config(init_dir, project)

    # check if project should have PIDs and start messaging thread
    pid_connector = None
    pid_prefix = config.check_pid_avail()
    if pid_prefix:
        pid_connector = establish_pid_connection(config,
                                                 pid_prefix,
                                                 test_publication)
        pid_connector.start_messaging_thread()

    # Connect to publisher DB
    dbcon = DBcon(config)

    # Load mapfile(s) and extract metadata
    if mapfile_dir is not None:
        for root, dirs, files in os.walk(mapfile_dir, followlinks=True):
            for mapfile in files:
                # publish data
                unpublish_dataset(dbcon,
                                os.path.join(root, mapfile),
                                config,
                                project,
                                unpublish_from_thredds,
                                unpublish_from_index,
                                pid_connector,
                                option)
    else:
        # publish data
        unpublish_dataset(dbcon,
                        mapfile,
                        config,
                        project,
                        unpublish_from_thredds,
                        unpublish_from_index,
                        pid_connector,
                        option)

    # update database
    dbcon.close()

    # Reinit THREDDS Server
    if reinitialize_thredds:
        reinit_thredds(config)

    if pid_connector:
        try:
            pid_connector.finish_messaging_thread()
        except:
            pid_connector.force_finish_messaging_thread()
            raise

if __name__ == '__main__':
    main(sys.argv[1:])
