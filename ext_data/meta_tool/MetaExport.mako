#!/bin/bash

# Installation directory, set to local installation directory
APP_HOME=$(dirname "$BASH_SOURCE") #"`pwd`"

# Java directory, uncomment and edit if global JAVA_HOME is not set
JAVA_HOME=${java_home}

# Try to locate java home from config if necessary
if [ ! -x "$JAVA_HOME/bin/java" ] ; then
  echo "Cannot find java binary. Please check if \$JAVA_HOME is set correctly!"
  exit 1
fi

# Start java application
$JAVA_HOME/bin/java -Xmx100m -cp $APP_HOME/MetaExport.jar:$APP_HOME/ojdbc5.jar de.dkrz.cera.applications.MetaExport "$@"
