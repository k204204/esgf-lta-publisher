import esgfpid
import socket


def establish_pid_connection(config, pid_prefix, test_publication):
    pid_messaging_service_exchange_name, pid_messaging_service_credentials, thredds_service_path, pid_data_node = config.get_pid_config()

    pid_connector = esgfpid.Connector(handle_prefix=pid_prefix,
                                      messaging_service_exchange_name=pid_messaging_service_exchange_name,
                                      messaging_service_credentials=pid_messaging_service_credentials,
                                      data_node=pid_data_node,
                                      thredds_service_path=thredds_service_path,
                                      test_publication=test_publication)
    return pid_connector


def pid_add_file(file, pid_wizard):
    # add file handle to dataset PID
    if pid_wizard:
        pid_wizard.add_file(file_name=file.filename,
                            file_handle=file.get_md('tracking_id'),
                            checksum=file.checksum,
                            file_size=file.size,
                            publish_path=file.get_md('HTTPServerURL'),
                            checksum_type=file.checksum_type,
                            file_version=file.get_md('version'))
