class Variable:

    def __init__(self, short_name, standard_name=None, long_name=None, units=None):
        self.short_name = short_name
        self.standard_name = standard_name
        self.long_name = long_name
        self.units = units
