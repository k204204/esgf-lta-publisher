class Dataset:
    def __init__(self, dataset_name=None, version=None, project=None, is_replica=None, pid=None, citation_url=None,
                 catalog_root='lta_dataroot'):
        self.md = {}
        self.dataset_name = dataset_name
        self.md['dataset_id'] = dataset_name
        self.md['drs_id'] = dataset_name
        self.dataset_version = version
        self.md['dataset_version'] = version
        self.is_replica = is_replica
        self.pid = pid
        self.description = dataset_name
        self.md['pid'] = pid
        self.citation_url = citation_url
        self.md['citation_url'] = citation_url
        self.project = project
        self.md['project'] = project
        self.catalog_location = None
        self.catalog_root = catalog_root
        self.lta = True
        self.files = set()

    def set_md(self, key, value):
        self.md[key.strip()] = value.strip()

    def get_md(self, key):
        return self.md[key]

    def add_file(self, f):
        self.files.add(f)
