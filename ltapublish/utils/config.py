import os
import urlparse
import ConfigParser
from utility import split_record


class Config:
    def __init__(self, init_dir, project):
        self.init_dir = init_dir
        self.project = project
        self.project_config_section = "config:%s" % project
        # load config
        default_config = os.path.join(init_dir, 'esg.ini')
        project_config = os.path.join(init_dir, 'esg.%s.ini' % project)
        self.config = ConfigParser.ConfigParser()
        self.config.read(default_config)
        self.config.read(project_config)

    def get(self, section, option, raw=False, variables={}):
        home = os.environ.get('HOME')
        if home is not None:
            variables['home'] = home
        return self.config.get(section, option, raw=raw, vars=variables)

    def has_section(self, section):
        return self.config.has_section(section)

    def check_pid_avail(self):
        try:
            if self.config.has_section(self.project_config_section):
                return self.config.get(self.project_config_section, 'pid_prefix')
        except:
            return None

    def get_pid_config(self):
        pid_messaging_service_exchange_name = None
        if self.config.has_section(self.project_config_section):
            pid_messaging_service_exchange_name = self.config.get(self.project_config_section, 'pid_exchange_name')
        if not pid_messaging_service_exchange_name:
            raise "Option 'pid_exchange_name' is missing in section '%s' of esg.ini." % self.project_config_section
        thredds_service_path = self.config.get(self.project_config_section, 'thredds_service_path')

        pid_data_node = urlparse.urlparse(self.config.get('DEFAULT', 'thredds_url')).netloc

        # get credentials from config:project section of esg.ini
        if self.config.has_section(self.project_config_section):
            pid_messaging_service_credentials = []
            credentials = split_record(self.config.get(self.project_config_section, 'pid_credentials'))
            if credentials:
                priority = 0
                for cred in credentials:
                    if len(cred) == 7 and isinstance(cred[6], int):
                        priority = cred[6]
                    elif len(cred) == 6:
                        priority += 1
                    else:
                        raise "Misconfiguration: 'pid_credentials', section '%s' of esg.ini." % self.project_config_section

                    ssl_enabled = False
                    if cred[5].strip().upper() == 'TRUE':
                        ssl_enabled = True

                    pid_messaging_service_credentials.append({'url': cred[0].strip(),
                                                              'port': cred[1].strip(),
                                                              'vhost': cred[2].strip(),
                                                              'user': cred[3].strip(),
                                                              'password': cred[4].strip(),
                                                              'ssl_enabled': ssl_enabled,
                                                              'priority': priority})

            else:
                raise "Option 'pid_credentials' is missing in section '%s' of esg.ini." % self.project_config_section
        else:
            raise "Section '%s' not found in esg.ini." % self.project_config_section

        return pid_messaging_service_exchange_name, pid_messaging_service_credentials, thredds_service_path, pid_data_node

    def get_citation_url(self, dataset_kv):
        citation = None
        if self.config.has_section(self.project_config_section):
            try:
                del dataset_kv['citation_url']
                citation = self.get(self.project_config_section, 'citation_url', variables=dataset_kv)
            except:
                pass
        return citation
