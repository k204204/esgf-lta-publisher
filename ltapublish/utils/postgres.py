import psycopg2


class DBcon:
    def __init__(self, config):
        dburl = config.get('DEFAULT', 'dburl')
        try:
            self.conn = psycopg2.connect(dburl)
        except:
            raise 'Unable to connect to the database'
        self.cur = self.conn.cursor()

    def update_postgres(self, dataset):
        statement = """INSERT INTO catalog values ('%s', '%s', '%s', '%s')""" % (dataset.dataset_name, dataset.dataset_version, dataset.catalog_location, dataset.catalog_root)
        self.cur.execute(statement)

    def delete(self, location):
        statement = """DELETE FROM catalog WHERE location = '%s'""" % location
        self.cur.execute(statement)

    def query_postgres(self, dataset, facet='*'):
        statement = """SELECT %s from catalog WHERE dataset_name='%s' AND version='%s'""" % (facet, dataset.dataset_name, dataset.dataset_version)
        self.cur.execute(statement)
        try:
            return self.cur.fetchone()[0]
        except:
            return None

    def return_all(self):
        statement = """SELECT * from catalog"""
        self.cur.execute(statement)
        return self.cur.fetchall()

    def close(self):
        self.conn.commit()
        self.cur.close()
        self.conn.close()


