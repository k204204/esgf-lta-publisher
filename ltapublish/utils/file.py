class File:
    def __init__(self, filename, filepath='', size='', checksum='', checksum_type=''):
        self.filename = filename.strip()
        self.filepath = filepath.strip()
        self.size = size.strip()
        self.checksum = checksum.strip()
        self.checksum_type = checksum_type.strip()
        self.md = {}
        self.variables = set()

    def set_md(self, key, value):
        self.md[key.strip()] = value.strip()

    def get_md(self, key):
        return self.md[key]

    def add_variable(self, variable):
        self.variables.add(variable)
