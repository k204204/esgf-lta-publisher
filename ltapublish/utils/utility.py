import re
import string


def split_record(option, sep='|'):
    result = []
    for record in option.split('\n'):
        if record == '':
            continue
        fields = map(string.strip, record.split(sep))
        result.append(fields)
    return result


def get_kv(pattern, string):
    kv = {}
    _patpat = re.compile(r'%\(([^()]*)\)s')  # Matches the %(name)s pattern
    idfields = [re.findall(_patpat, format) for format in [pattern]][0]
    pat = re.sub(_patpat, r'(?P<\1>[^.]*)', pattern.strip())
    res = re.search(pat, string)

    for key in idfields:
        kv[key] = res.group(key)

    return kv


def get_model_description(model):
    models_table = open('/esg/config/esgcet/esgcet_models_table.txt')
    for line in models_table:
        if model in line:
            return line.split('|')[-1].strip()
    return None


def get_experiment_description(experiment, map_raw_experiment_options):
    for line in map_raw_experiment_options.split('\n'):
        if experiment in line:
            return line.split('|')[-1].strip()
    return None
