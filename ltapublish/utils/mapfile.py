import os

from dataset import Dataset
from file import File
from variable import Variable
from utility import get_kv, get_experiment_description, get_model_description


def load_mapfile(mapfile_path, project, config, pid_connector):
    mapfile = open(mapfile_path)
    dset = Dataset(project=project)
    pid_wizard = None

    # get the list of required metadata
    dataset_required_metadata = [i.strip() for i in config.get(config.project_config_section, 'dataset_metadata').split(',')]

    # get cera acronym from mapfile path (needed for CMIP5 citation)
    if 'cera_acronym' in dataset_required_metadata:
        dset.set_md('cera_acronym', os.path.basename(os.path.dirname(mapfile_path)))

    for line in mapfile:
        if line[0] == '#' or line.strip() == '':
            continue
        line_parts = line.split('|')

        if line_parts[0].strip() == 'DATASET':
            dataset_name, dataset_version = line_parts[1].strip().split('#')
            is_replica = True  # TODO: currently only support replicas
            dset.dataset_name = dataset_name
            dset.dataset_version = dataset_version
            dset.is_replica = is_replica

            if pid_connector:
                pid_wizard = pid_connector.create_publication_assistant(drs_id=dataset_name,
                                                                        version_number=dataset_version,
                                                                        is_replica=is_replica)

            for pair in line_parts[2:]:
                if 'description=' in pair:
                    dset.description = pair.strip().split('description=')[-1]
                else:
                    key, value = pair.strip().split('=')
                    if key == 'pid':
                        dset.pid = value
                    else:
                        dset.set_md(key, value)

            # add citation, if required
            citation = config.get_citation_url(dset.md)
            if citation:
                dset.citation_url = citation

        elif line_parts[0].strip() == 'FILE':
            filename, filepath, size, checksum, checksum_type = line_parts[1:6]
            map_file = File(filename, filepath, size, checksum, checksum_type)
            var = ''
            for pair in line_parts[6:]:
                if len(pair.strip().split('=')) == 2:
                    key, value = pair.strip().split('=')
                else:
                    key = pair.strip().split('=')[0]
                    value = pair.strip().split(key)[1]
                if key == 'variable':
                    short_name, standard_name, long_name, units = value.split(',')
                    var = Variable(short_name, standard_name, long_name, units)
                else:
                    map_file.set_md(key, value)
                    if key in dataset_required_metadata and key not in dset.md:
                        dset.set_md(key, value)

            map_file.add_variable(var)
            dset.add_file(map_file)

    # try to get missing md from config file
    for md in dataset_required_metadata:
        if md not in dset.md:
            print md
#            dset.set_md(key, value)
            # TODO
    # add required metadata for cmip5
    dset.set_md('dataset_id_template_', 'cmip5.%(product)s.%(institute)s.%(model)s.%(experiment)s.%(time_frequency)s.%(realm)s.%(cmor_table)s.%(ensemble)s')


    return dset, pid_wizard


def write_mapfile(config, dataset, files, project, is_replica, out_dir, tmp_dir_location):
    mapfile_openfile = open(os.path.join(out_dir, '%s.map' % dataset).replace('#', '.v'), 'w')
    [dataset_name, dataset_version] = dataset.split('#')

    config_pattern_dataset_id = config.get('project:%s' % project, 'dataset_id', raw=True)
    dataset_metadata = [k.strip() for k in config.get(config.project_config_section, 'dataset_metadata').split(',')]

    dataset_kv = get_kv(config_pattern_dataset_id, dataset_name)

    mapfile_dataset = Dataset(dataset_name, dataset_version, project, is_replica)

    for key, value in dataset_kv.iteritems():
        mapfile_dataset.set_md(key, value)

    # set description
    dataset_kv_all = {
        'version': mapfile_dataset.dataset_version,
        'project_description': 'CMIP5',
        'model_description': (get_model_description(dataset_kv['model']) or dataset_kv['model']),
        'experiment_description': (get_experiment_description(dataset_kv['experiment'], config.get('project:%s' % project, 'experiment_options')) or dataset_kv['experiment'])
        }
    dataset_kv_all.update(dataset_kv)
    mapfile_dataset.set_md('description', config.get('project:%s' % project, 'dataset_name_format', raw=False, variables=dataset_kv_all))

    for f in files:
        xml_filename = '%s.xml' % f
        openfile_file_metadata = open(os.path.join(tmp_dir_location, xml_filename))
        filename = '%s.nc' % f.split('.')[-2]
        # extract variable from filename TODO: Support filenames not matching the variable_xx_xx_xx.nc format
        var = Variable(filename.split('_')[0])
        mapfile_file = File(filename)
        for line in openfile_file_metadata:
            if '<field name="' in line:
                try:
                    [key, value] = line.split('<field name="')[-1].split('</field>')[0].split('">')
                    if value == 'not filled':
                        continue
                    elif key == 'size':
                        mapfile_file.size = value
                    elif key == 'checksum':
                        mapfile_file.checksum = value
                    elif key == 'checksum_type':
                        mapfile_file.checksum_type = value
                    elif key == 'cf_standard_name':
                        var.standard_name = value
                        var.long_name = value  # TODO: Support standard_name!=long_name
                    elif key == 'units':
                        var.units = value
                    # ignore citation stuff ... (previously used)
                    # also skip instance_id and master_id
                    elif key == 'doi' or 'doi_' in key or 'std_' in key or 'instance_id' in key or 'master_id' in key:
                        continue
                    else:
                        mapfile_file.set_md(key, value)
                except:
                    pass
        mapfile_file.add_variable(var)
        # set file version
        mapfile_file.set_md('file_version', '1')
        mapfile_dataset.add_file(mapfile_file)

        file_root = config.get(config.project_config_section, 'file_root')
        mapfile_file.filepath = os.path.join(file_root, '%s.nc' % f.split('.nc')[0].replace('.', '/'))

        # add files to mapfile
        line = 'FILE | %s | %s | %s | %s | %s' \
               % (filename, mapfile_file.filepath, mapfile_file.size, mapfile_file.checksum, mapfile_file.checksum_type)
        line += ' | variable=%s,%s,%s,%s' \
                % (var.short_name, var.standard_name, var.long_name, var.units)
        for key, value in mapfile_file.md.iteritems():
            if value:
                line += ' | %s=%s' % (key, value)
        mapfile_openfile.write('%s\n' % line)

    # add datasets to mapfile
    line = 'DATASET | %s#%s ' % (dataset_name, dataset_version)
    for key, value in mapfile_dataset.md.iteritems():
        if value:
            line += ' | %s=%s' % (key, value)
    for key in dataset_metadata:
        key = key.strip()
        if key not in mapfile_dataset.md:
            try:
                if mapfile_file.get_md(key):
                    line += ' | %s=%s' % (key, mapfile_file.get_md(key))
            except:
                print 'WARNING\tField not found:', key

    mapfile_openfile.write('%s\n' % line)

    mapfile_openfile.close()
