import os
import ssl
import httplib
import urlparse
import datetime
from base64 import b64encode
from ltapublish.utils import split_record, pid_add_file

from lxml.etree import Element, SubElement as SE, ElementTree


_nsmap = {
    None: 'http://www.unidata.ucar.edu/namespaces/thredds/InvCatalog/v1.0',
    'xlink': 'http://www.w3.org/1999/xlink',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}
_XSI = '{%s}' % _nsmap['xsi']
_XLINK = '{%s}' % _nsmap['xlink']

DEFAULT_THREDDS_CATALOG_VERSION = '2'

DEFAULT_THREDDS_SERVICE_APPLICATIONS = {
    'GridFTP': ['DataMover-Lite'],
    'HTTPServer': ['Web Browser', 'Web Script'],
    'OpenDAP': ['Web Browser'],
    'Globus': ['Web Browser']
    }

DEFAULT_THREDDS_SERVICE_AUTH_REQUIRED = {
    'GridFTP': 'true',
    'HTTPServer': 'true',
    'OpenDAP': 'false',
    'Globus': 'false',
    }

DEFAULT_THREDDS_SERVICE_DESCRIPTIONS = {
    'GridFTP': 'GridFTP',
    'HTTPServer': 'HTTPServer',
    'OpenDAP': 'OpenDAP',
    'Globus': 'Globus Transfer Service',
    }

THREDDS_BASES = ['/thredds/fileServer', '/thredds/dodsC', '/thredds/wcs', '/thredds/ncServer']

THREDDS_FILESERVER = '/thredds/fileServer'


def reinit_thredds(config):
    print 'Reinitializing THREDDS: ',
    thredds_reinit_url = config.get('DEFAULT', 'thredds_reinit_url')
    thredds_username = config.get('DEFAULT', 'thredds_username')
    thredds_password = config.get('DEFAULT', 'thredds_password')

    # Create an OpenerDirector with support for Basic HTTP Authentication...
    try:
        ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    except AttributeError:
        ctx = ssl.SSLContext(ssl.PROTOCOL_TLS)


    user_pass = b64encode(thredds_username +":"+thredds_password)
    pd = urlparse.urlparse(thredds_reinit_url)
    conn = httplib.HTTPSConnection(pd.hostname, port=pd.port, context=ctx)
    req_headers = {'Host': pd.hostname, 'Authorization': 'Basic %s' % user_pass}
    conn.request('GET', thredds_reinit_url, headers=req_headers)
    res = conn.getresponse()
    reinit_result = res.read()
    if 'reinit ok' in reinit_result:
        print 'SUCCESS'
    else:
        print 'FAIL: %s' % reinit_result
    conn.close()

    # result = requests.get(thredds_reinit_url, auth=requests.auth.HTTPBasicAuth(thredds_username, thredds_password))

    # if not result.status_code == '200':
        # result.raise_for_status()


def generate_thredds_service(config, catalog):
    thredds_file_specs_cfg = split_record(config.get('DEFAULT', 'thredds_file_services'))

    service_dict = {}
    return_service_dict = {}

    # Create the elements
    for service_type, base, name, compound_name in thredds_file_specs_cfg:
        is_thredds_service = (os.path.normpath(base) in THREDDS_BASES)
        is_thredds_fileservice = (os.path.normpath(base) == THREDDS_FILESERVER)
        desc = DEFAULT_THREDDS_SERVICE_DESCRIPTIONS[service_type]

        # If the service is part of a compound service:
        if compound_name is not None:

            # Create the compound service if necessary
            compound_service = service_dict.get(compound_name, None)
            if compound_service is None:
                compound_service = SE(catalog, 'service', name=compound_name, serviceType='Compound', base='')
                service_dict[compound_name] = compound_service
                return_service_dict[compound_name] = (False, [])

            # Append the simple service to the compound
            service = SE(compound_service, 'service', name=name, serviceType=service_type, base=base, desc=desc)
            service_dict[name] = service
            return_service_dict[compound_name][1].append(name)
            return_service_dict[name] = (True, is_thredds_service, service_type, base, is_thredds_fileservice)

        # Otherwise if a simple service just create the service
        else:
            if service_dict.has_key(name):
                raise 'Duplicate service name in configuration: %s' % name
            service = SE(catalog, 'service', name=name, serviceType=service_type, base=base, desc=desc)
            return_service_dict[compound_name] = (True, is_thredds_service, service_type, base, is_thredds_fileservice)

        # Add the requires_authorization and application properties
        auth_required = DEFAULT_THREDDS_SERVICE_AUTH_REQUIRED[service_type]
        SE(service, 'property', name='requires_authorization', value=auth_required)

        apps = DEFAULT_THREDDS_SERVICE_APPLICATIONS[service_type]
        for app in apps:
            SE(service, 'property', name='application', value=app)

    return return_service_dict


def generate_thredds_catalog_output_path(config, dset_object):
    thredds_root = config.get('DEFAULT', 'thredds_root')
    thredds_catalog_name = '%s.v%s.xml' % (dset_object.dataset_name, dset_object.dataset_version)
    thredds_max_catalogs = int(config.get('DEFAULT', 'thredds_max_catalogs_per_directory'))

    # Get the subdirectory name:
    # - Find the 'last' subdirectory in numerical order
    subdirs = os.listdir(thredds_root)
    num_subdirs = [int(item) for item in subdirs if item.isdigit()]
    if len(num_subdirs) > 0:
        nsubdirs = max(num_subdirs)
        subdir = str(nsubdirs)
        last_subdir = os.path.join(thredds_root, subdir)

        # - Get the number of catalogs. If >= the max, create a new directory
        ncatalogs = len(os.listdir(last_subdir))
        if ncatalogs >= thredds_max_catalogs:
            subdir = str(nsubdirs + 1)
            last_subdir = os.path.join(thredds_root, subdir)
    else:
        subdir = '1'
        last_subdir = os.path.join(thredds_root, subdir)

    # Create the subdirectory if necessary
    if not os.path.exists(last_subdir):
        try:
            os.mkdir(last_subdir, 0775)
        except OSError:
            if not os.path.exists(last_subdir):
                print 'Error creating directory %s. Please make sure you have the correct permissions.' % last_subdir
                raise

    # Build the catalog name
    dset_object.catalog_location = os.path.join(subdir, thredds_catalog_name)
    return os.path.join(last_subdir, thredds_catalog_name)


def get_url_path(config, dset_object, filepath):
    thredds_roots_cfg = split_record(config.get('DEFAULT', 'thredds_dataset_roots'))
    for key, value in thredds_roots_cfg:
        if value in filepath:
            filepath = filepath.replace(value, key)
            dset_object.catalog_root = key
            break
    return filepath


def add_file_to_catalog(config, dset_element, dset_object, file_object, service_dict, service_name):

    file_full_id = '%s.v%s.%s' % (dset_object.dataset_name, dset_object.dataset_version, file_object.filename)
    dataset = SE(dset_element, 'dataset', name=file_object.filename, ID=file_full_id)
    file_id = '%s.%s' % (dset_object.dataset_name, file_object.filename)
    SE(dataset, 'property', name='file_id', value=file_id)
    SE(dataset, 'property', name='size', value=str(file_object.size))
    SE(dataset, 'property', name='checksum', value=file_object.checksum)
    SE(dataset, 'property', name='checksum_type', value=file_object.checksum_type)
    # set lta flag
    SE(dataset, 'property', name='lta', value='true')

    for key, value in file_object.md.iteritems():
        SE(dataset, 'property', name=key, value=value)

    variable_parent = SE(dataset, 'variables', vocabulary='CF-1.0')
    for variable in file_object.variables:
        generate_variable(variable_parent, variable)
    data_size = SE(dataset, 'dataSize', units='bytes')
    data_size.text = str(file_object.size)

    service_description = service_dict[service_name]
    if service_description[0]:  # Simple service
        is_simple, is_thredds_service, service_type, base, is_thredds_fileservice = service_description
        # For restrictAccess to work, the access info has to be in the dataset element!
        url_path = get_url_path(config, dset_object, file_object.filepath)
        if is_thredds_fileservice:
            dataset.set('urlPath', url_path)
            dataset.set('serviceName', service_name)
        else:
            if service_name == 'GRIDFTP':
                SE(dataset, 'access', serviceName=service_name, urlPath=os.path.join('/', url_path))
            else:
                SE(dataset, 'access', serviceName=service_name, urlPath=url_path)
    else:  # Compound service
        is_simple, service_list = service_description
        for sub_name in service_list:
            is_simple, is_thredds_service, service_type, base, is_thredds_fileservice = service_dict[sub_name]
            url_path = get_url_path(config, dset_object, file_object.filepath)
            if sub_name == 'HTTPServer':
                file_object.set_md('HTTPServerURL', url_path)
            if is_thredds_service:
                dataset.set('urlPath', url_path)
                dataset.set('serviceName', sub_name)
            else:
                if sub_name == 'GRIDFTP':
                    SE(dataset, 'access', serviceName=sub_name, urlPath=os.path.join('/', url_path))
                else:
                    SE(dataset, 'access', serviceName=sub_name, urlPath=url_path)


def generate_variable(parent, var):
    variable_element = SE(parent, 'variable', name=var.short_name)
    variable_element.set('vocabulary_name', var.standard_name)
    variable_element.set('vocabulary_name', var.long_name)
    variable_element.set('units', var.units)
    variable_element.text = var.long_name


def generate_dataset_catalog(dbcon, config, dset_object, pid_wizard):

    # initial setup of the THREDDS dataset catalog
    catalog = Element('catalog', name='TDS configuration file', nsmap=_nsmap)
    catalog.set(_XSI+'schemaLocation', '%s http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.1.0.2.xsd' % _nsmap[None])
    doc = ElementTree(catalog)

    # generate all thredds file services
    service_dict = generate_thredds_service(config, catalog)

    # set the catalog version number
    SE(catalog, 'property', name='catalog_version', value=DEFAULT_THREDDS_CATALOG_VERSION)

    # initialize dataset element with dataset_name and version
    dataset_id = '%s.v%s' % (dset_object.dataset_name, dset_object.dataset_version)
    dset_element = SE(catalog, 'dataset', name=dset_object.description, ID=dataset_id)
    thredds_restrict_access = config.get('DEFAULT', 'thredds_restrict_access')
    dset_element.set('restrictAccess', thredds_restrict_access)

    # set pid
    if dset_object.pid and dset_object.pid != 'None':
        SE(dset_element, 'property', name='pid', value=dset_object.pid)
        documentation = SE(dset_element, 'documentation', type='pid')
        documentation.set(_XLINK + 'href', 'http://hdl.handle.net/%s' %dset_object.pid)
        documentation.set(_XLINK + 'title', 'PID')

    # set citation
    if dset_object.citation_url and dset_object.citation_url != 'None':
        SE(dset_element, 'property', name='citation_url', value=dset_object.citation_url)
        documentation = SE(dset_element, 'documentation', type='citation')
        documentation.set(_XLINK + 'href', dset_object.citation_url)
        documentation.set(_XLINK + 'title', 'Citation')

    # set creation time
    SE(dset_element, 'property', name='creation_time', value=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    # set replica flag
    if dset_object.is_replica:
        SE(dset_element, 'property', name='is_replica', value='true')

    # set lta flag
    SE(dset_element, 'property', name='lta', value='true')

    # set all other metadata
    for key, value in dset_object.md.iteritems():
        if key != 'home' and value:
            SE(dset_element, 'property', name=key, value=value)

    # add ESGF required metadata
    thredds_metadata = [i.strip() for i in config.get('DEFAULT', 'dataset_esgf_metadata').split(',')]


    metadata_element = SE(dset_element, 'metadata')
    variables = SE(metadata_element, 'variables', vocabulary='CF-1.0')

    variable_set = set()
    for ds_file in dset_object.files:
        for var in ds_file.variables:
            if var.short_name not in variable_set:
                generate_variable(variables, var)
                variable_set.add(var.short_name)

    metadata_element = SE(dset_element, 'metadata', inherited='true')
    data_type = SE(metadata_element, "dataType")
    data_type.text = "Grid"
    data_format = SE(metadata_element, "dataFormat")
    data_format.text = "NetCDF"

    # add files
    for file_object in dset_object.files:
        service_name = 'lta_fileservice'
        add_file_to_catalog(config, dset_element, dset_object, file_object, service_dict, service_name)
        pid_add_file(file_object, pid_wizard)

    # generate output file and write catalog
    catalog_location = generate_thredds_catalog_output_path(config, dset_object)
    dbcon.update_postgres(dset_object)
    doc.write(catalog_location, xml_declaration=True, encoding='UTF-8', pretty_print=True)


def remove_dataset_catalog(dbcon, config, location, pid_wizard):
    # remove dataset catalog
    thredds_root = config.get('DEFAULT', 'thredds_root')
    thredds_catalog_path = os.path.join(thredds_root, location)
    os.remove(thredds_catalog_path)

    # update postgres
    dbcon.delete(location)

    # update THREDDS root catalog
    update_root_catalog(dbcon, config)


def update_root_catalog(dbcon, config):
    global _nsmap, _XSI

    # Get the catalog path
    thredds_root = config.get('DEFAULT', 'thredds_root')
    thredds_master_catalog_name = config.get('DEFAULT', 'thredds_master_catalog_name')
    thredds_dataset_roots_option = config.get('DEFAULT', 'thredds_dataset_roots')
    thredds_dataset_roots_specs = split_record(thredds_dataset_roots_option)
    thredds_dataset_root_paths = [item[0] for item in thredds_dataset_roots_specs]
    master = os.path.join(thredds_root, "catalog.xml")

    # Generate the master catalog
    catalog = Element('catalog', name=thredds_master_catalog_name, nsmap=_nsmap)
    catalog.set(_XSI + 'schemaLocation', '%s http://www.unidata.ucar.edu/schemas/thredds/InvCatalog.1.0.2.xsd' % _nsmap[None])

    # Create document root elements
    for path, location in thredds_dataset_roots_specs:
        SE(catalog, "datasetRoot", path=path, location=location)
    doc = ElementTree(catalog)

    # Get the dataset catalogs.
    catalog_entries = dbcon.return_all()
    for (dataset_name, version, location, rootpath) in catalog_entries:
        if rootpath is not None and rootpath not in thredds_dataset_root_paths:
            print 'Please add rootpath %s to thredds_dataset_roots in the configuration file.' % rootpath
        else:
            catalog_id = "%s.v%d" % (dataset_name, version)
            catalog_ref = SE(catalog, "catalogRef", name=catalog_id)
            catalog_ref.set(_XLINK + "title", catalog_id)
            catalog_ref.set(_XLINK + "href", location)

    doc.write(master, xml_declaration=True, encoding='UTF-8', pretty_print=True)


def publish_thredds_catalog(dbcon, config, dset_object, pid_wizard):
    generate_dataset_catalog(dbcon, config, dset_object, pid_wizard)
    update_root_catalog(dbcon, config)


def unpublish_thredds(dbcon, config, dset_object, pid_wizard):
    remove_dataset_catalog(dbcon, config, dset_object, pid_wizard)
    update_root_catalog(dbcon, config)