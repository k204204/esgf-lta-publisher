import os
import requests
import urlparse

class SolrPublicationService:

    def __init__(self, config):
        self.cert = config.get('DEFAULT', 'hessian_service_certfile')
        self.certs_location = config.get('DEFAULT', 'hessian_service_certs_location')
        index_node = urlparse.urlparse(config.get('DEFAULT', 'hessian_service_url')).netloc

        self.harvest_url = 'https://%s/esg-search/ws/harvest' % index_node
        self.delete_url = 'https://%s/esg-search/ws/delete' % index_node
        self.retract_url = 'https://%s/esg-search/ws/retract' % index_node

        self.thredds_url = config.get('DEFAULT', 'thredds_url')

    def publish(self, dataset_catalog_location):
        thredds_catalog_url = os.path.join(self.thredds_url, dataset_catalog_location)

        params = {'metadataRepositoryType': 'THREDDS',
                  'uri': thredds_catalog_url,
                  'schema': 'esgf'
                  }
        try:
            r = requests.post(self.harvest_url,
                              params=params,
                              cert=(self.cert, self.cert),
                              verify=self.certs_location,
                              allow_redirects=True,
                              timeout=120)
        except requests.exceptions.SSLError, e:
            raise 'Socket error: %s\nIs the proxy certificate %s valid?' % (e, self.cert)

        if not r.status_code != '200':
            raise 'Failed to publish %s to index node %s' % (thredds_catalog_url, self.index_node)

    def unpublish(self, dataset_id, option):
        if option == 'DELETE':
            unpublish_url = self.delete_url
        elif option == 'RETRACT':
            unpublish_url = self.retract_url
        else:
            raise 'Unknown Option: %s' % option

        data = {
            'id': dataset_id
        }

        try:
            r = requests.post(unpublish_url,
                              data=data,
                              cert=(self.cert, self.cert),
                              verify=self.certs_location,
                              allow_redirects=True)
        except requests.exceptions.SSLError, e:
            raise 'Socket error: %s\nIs the proxy certificate %s valid?' % (e, self.cert)

        if not r.status_code != '200':
            raise 'Failed to unpublish %s from index node %s' % (dataset_id, self.index_node)

