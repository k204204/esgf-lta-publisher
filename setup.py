#!/usr/bin/env python

import os
from shutil import copyfile
from setuptools import setup, find_packages
from setuptools.command.install import install
import site

VERSION = '0.0.0'


class PostInstallCommand(install):

    def run(self):
        from subprocess32 import check_output
        from mako.template import Template
        meta_export_tool_template = Template(filename='ext_data/meta_tool/MetaExport.mako')

        # workaround to get JAVA_HOME
        meta_export_tool_java_cmd = 'find /usr/lib/ -name jre | sort -r | head -1'
        meta_export_tool_java = check_output(meta_export_tool_java_cmd, shell=True)
        if not meta_export_tool_java:
            meta_export_tool_java_cmd = 'find /usr/local/ -name jre | sort -r | head -1'
            meta_export_tool_java = check_output(meta_export_tool_java_cmd, shell=True)
        # dirty hack to get installation path
        # TODO!: need better way to do this
        # import sys
        # for path in sys.path:
        #     if 'ltapublish-' in path and '.egg' in path and 'dist-packages' in path:
        #         me_path = os.path.join(path, 'ext_data/meta_tool')
        #         if not os.path.isdir(me_path):
        #             os.mkdir(me_path, 775)
        #         break

        path = site.getsitepackages()[0]
        me_path = os.path.join(path, 'ext_data/meta_tool')
        if not os.path.isdir(me_path):
            os.mkdir(me_path, 775)
        me_ffp = me_path + '/MetaExport'
        meta_export_tool = open(me_ffp, 'w')
        meta_export_tool.write(meta_export_tool_template.render(java_home=meta_export_tool_java))
        meta_export_tool.close()
        os.chmod(me_ffp, 0755)

        # copy jars to install_dir
        for root, _, files in os.walk('ext_data/meta_tool/'):
            for f in files:
                if '.jar' in f:
                    copyfile(os.path.join(root, f), os.path.join(me_path, f))

        install.run(self)

setup(
    name='ltapublish',
    version=VERSION,
    description='DKRZ LTA to ESGF publication package',
    author='Katharina Berger (DKRZ)',
    author_email='berger@dkrz.de',
    setup_requires=[
        "mako>=1.0.7",
    ],
    install_requires=[
        "esgfpid>=0.7.10",
        "MyProxyClient>=2.0.3",
        "subprocess32>=3.5.3",
    ],
    packages=find_packages(),
    include_package_data=True,
    package_data={
        'ext_data.meta_tool': ['*'],
    },
    scripts=[
        'scripts/ltagenacronyms',
        'scripts/ltamapfile',
        'scripts/ltapublish',
        'scripts/ltaunpublish',
        'scripts/reinit-thredds',
    ],
    zip_safe=False,
    cmdclass={
        'install': PostInstallCommand,
    },
)
